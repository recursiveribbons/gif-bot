package dev.robinsyl.gifbot.services;

import dev.robinsyl.gifbot.types.State;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.User;

import java.util.HashMap;
import java.util.Optional;

@Service
public class StateService {

    private final HashMap<Long, Entry> states = new HashMap<>();

    public void setState(User user, State state) {
        states.put(user.getId(), new Entry(state));
    }

    public void setState(User user, State state, Object data) {
        if (!state.getDataType().isInstance(data)) {
            throw new IllegalStateException("The data given does not match the required data type for this state.");
        }
        states.put(user.getId(), new Entry(state, data));
    }

    public Optional<State> getState(User user) {
        Entry entry = states.get(user.getId());
        if (entry == null) {
            return Optional.empty();
        }
        return Optional.of(entry.getState());
    }

    public Optional<Object> getData(User user) {
        Entry entry = states.get(user.getId());
        if (entry == null) {
            return Optional.empty();
        }
        return Optional.ofNullable(entry.getData());
    }

    public void removeState(User user) {
        states.remove(user.getId());
    }

    public void clearStates() {
        states.clear();
    }

    public int getCount() {
        return states.size();
    }

    private static class Entry {
        private final State state;
        private final Object data;

        Entry(State state) {
            this.state = state;
            this.data = null;
            if (state.hasData()) {
                throw new IllegalStateException("This state requires data.");
            }
        }

        Entry(State state, Object data) {
            this.state = state;
            this.data = data;
            if (!state.hasData()) {
                throw new IllegalStateException("This state does not allow data.");
            }
        }

        State getState() {
            return state;
        }

        Object getData() {
            return data;
        }

    }
}
