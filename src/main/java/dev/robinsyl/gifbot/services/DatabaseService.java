package dev.robinsyl.gifbot.services;

import dev.robinsyl.gifbot.types.TaggedGif;
import io.micrometer.core.instrument.MeterRegistry;
import jakarta.annotation.PreDestroy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.games.Animation;

import java.io.Closeable;
import java.sql.*;
import java.util.*;

@Service
public class DatabaseService implements Closeable {
    private static final Logger logger = LoggerFactory.getLogger(DatabaseService.class);

    private final Connection con;

    public DatabaseService(@Value("jdbc:postgresql://${gifs.db.url}:${gifs.db.port}/${gifs.db.name}") String url, @Value("${gifs.db.user}") String user, @Value("${gifs.db.password}") String password, MeterRegistry meterRegistry) {
        int tries = 1;
        int timeout = 500;
        Connection connection;
        logger.info("Using connection string {}", url);
        while (true) {
            try {
                connection = DriverManager.getConnection(url, user, password);
                break;
            } catch (SQLException e) {
                if (tries++ > 10) {
                    throw new BeanInitializationException("Could not get connection, giving up.", e);
                }
                logger.warn("Could not get connection, try {}", tries);
            }
            try {
                Thread.sleep(timeout);
            } catch (InterruptedException e) {
                logger.error("Thread interrupted", e);
                Thread.currentThread().interrupt();
            }
            timeout *= 2;
        }
        this.con = connection;
        logger.info("Connected to database");
        try (Statement stmt = con.createStatement()) {
            stmt.executeUpdate("create table if not exists users(id bigint primary key, first_name varchar(32) not null, last_name varchar(32), username varchar(32), language_code varchar(16));");
            stmt.executeUpdate("create table if not exists gifs(file_unique_id varchar(32) primary key, file_id varchar(128) not null, set_name varchar(32))");
            stmt.executeUpdate("create table if not exists tags(user_id bigint constraint tags_users_id_fk references users on delete cascade, gif_id varchar(32) constraint tags_gifs_id_fk references gifs, tag varchar(16) not null, constraint tags_pk primary key (user_id, gif_id, tag));");
            logger.info("Database has been initialised.");
        } catch (SQLException e) {
            throw new BeanInitializationException("Could not initialise database", e);
        }
        meterRegistry.gauge("bot.users", this, DatabaseService::getUserCount);
        meterRegistry.gauge("bot.gifs", this, DatabaseService::getGifCount);
        meterRegistry.gauge("bot.tags", this, DatabaseService::getTagCount);
    }

    @PreDestroy
    public void close() {
        try {
            con.close();
        } catch (SQLException e) {
            logger.info("Could not close connection", e);
        }
    }

    // Gif functionality

    /**
     * This method returns the file ID of gifs tagged by a user that matches the given tags, limited to 50 gifs.
     * The tags are matched by prefix, so "foo" will match a gifs tagged with "foobar". Tag matching is case insensitive.
     * Results are sorted by the number of matched tags.
     * An empty set is returned if no tags are given.
     *
     * @param userId The ID of the user
     * @param offset Offset
     * @param tags   The tags to query, should not be empty
     * @return A {@link List<String>} of file IDs of the gifs tagged by the user. This set is empty if there are no results found, or if there is a database error.
     */
    public List<String> getGifFileIds(long userId, int offset, String... tags) {
        if (tags.length == 0) {
            return Collections.emptyList();
        }
        List<String> results = new LinkedList<>();

        // Build query

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < tags.length - 1; i++) {
            // WHERE (tag ILIKE ? OR tag ILIKE ? ...)
            sb.append(" OR tag ILIKE ?");
        }
        String or = sb.toString();

        try (PreparedStatement stmt = con.prepareStatement("SELECT file_id FROM (SELECT gif_id, count(*) as weight FROM tags t WHERE user_id=? AND (tag ILIKE ?" + or + ") GROUP BY gif_id ORDER BY weight DESC LIMIT 50 OFFSET ?) t INNER JOIN gifs g ON t.gif_id = g.file_unique_id;")) {
            int i = 1;
            stmt.setLong(i++, userId); // WHERE user_id = ?
            for (String tag : tags) {
                // WHERE tag ILIKE 'syl%'
                stmt.setString(i++, tag.toLowerCase() + '%');
            }
            stmt.setInt(i, offset); // OFFSET ?

            // Execute and access results

            boolean hasNext = stmt.execute();
            while (hasNext) {
                try (ResultSet rs = stmt.getResultSet()) {
                    while (rs.next()) {
                        results.add(rs.getString("file_id"));
                    }
                    hasNext = stmt.getMoreResults();
                }
            }
        } catch (SQLException e) {
            logger.error("Database error in getgifFileIds!", e);
            return Collections.emptyList();
        }
        return results;
    }

    /**
     * This method returns a {@link TaggedGif} object of a gif tagged by the user.
     *
     * @param userId The ID of the user
     * @param gifId  The ID of the gif
     * @return An {@link Optional<TaggedGif>} containing the gif. This optional is empty if there is no result found, or if there is a database error.
     */
    public Optional<TaggedGif> getTaggedGif(long userId, String gifId) {
        TaggedGif gif;

        try (PreparedStatement stmt = con.prepareStatement("SELECT g.file_unique_id, t.tag_array, g.file_id FROM (SELECT gif_id, array_agg(tag) AS tag_array FROM tags WHERE user_id=? AND gif_id=? GROUP BY gif_id) t INNER JOIN gifs g ON g.file_unique_id = t.gif_id;")) {
            stmt.setLong(1, userId); // WHERE user_id = ?
            stmt.setString(2, gifId); // WHERE gif_id = ?

            // Execute and access results

            boolean result = stmt.execute();
            if (!result) {
                return Optional.empty();
            }
            try (ResultSet rs = stmt.getResultSet()) {
                if (!rs.next()) {
                    return Optional.empty();
                }
                List<String> tags = Arrays.asList((String[]) rs.getArray("tag_array").getArray());
                gif = new TaggedGif(rs.getString("file_unique_id"), rs.getString("file_id"), tags);
            }
        } catch (SQLException e) {
            logger.error("Database error in getgif!", e);
            return Optional.empty();
        }
        return Optional.of(gif);
    }

    /**
     * This method adds any number of tags to a given gif for a particular user.
     * If no tags are given, this method does nothing.
     * This method calls {@link #addOrUpdateGif(Animation)} to ensure that the gif exists in the database.
     *
     * @param userId The ID of the user
     * @param gif    The {@link Animation} to be tagged
     * @param tags   The tags to add, should not be empty
     */
    public void addTags(long userId, Animation gif, String... tags) {
        if (tags.length == 0) {
            return;
        }
        try (PreparedStatement stmt = con.prepareStatement("INSERT INTO tags(user_id, gif_id, tag) VALUES(?,?,?);")) {
            addOrUpdateGif(gif);
            for (String tag : tags) {
                try {
                    stmt.setLong(1, userId);
                    stmt.setString(2, gif.getFileUniqueId());
                    stmt.setString(3, tag);
                    stmt.executeUpdate();
                } catch (SQLException e) {
                    logger.info("Error in addTags, possibly an attempt to add a duplicate tag", e);
                }
            }
        } catch (SQLException e) {
            logger.error("Database error in addTags!", e);
        }
    }

    /**
     * This method removes any number of tags from the given gif for the given user.
     *
     * @param userId The ID of the user
     * @param gifId  The ID of the gif to remove
     * @param tags   The tags to remove from the gif
     */
    public void removeTags(long userId, String gifId, String... tags) {
        if (tags.length == 0) {
            return;
        }
        try (PreparedStatement stmt = con.prepareStatement("DELETE FROM tags WHERE user_id=? AND gif_id=? AND tag=?;")) {
            for (String tag : tags) {
                stmt.setLong(1, userId);
                stmt.setString(2, gifId);
                stmt.setString(3, tag);
                stmt.executeUpdate();
            }
        } catch (SQLException e) {
            logger.error("Database error in removeTags!", e);
        }
    }

    /**
     * This method returns the tags assigned to the given gif by the given user.
     *
     * @param userId The ID of the user
     * @param gifId  The ID of the gif
     * @return The {@link Set<String>} of tags that are assigned by the user to the gif
     */
    public Set<String> getTags(long userId, String gifId) {
        Set<String> results = new HashSet<>();

        try (PreparedStatement stmt = con.prepareStatement("SELECT tag FROM tags WHERE user_id=? AND gif_id=?")) {
            stmt.setLong(1, userId);
            stmt.setString(2, gifId);

            // Execute and access results

            boolean hasNext = stmt.execute();
            while (hasNext) {
                try (ResultSet rs = stmt.getResultSet()) {
                    while (rs.next()) {
                        results.add(rs.getString("tag"));
                    }
                    hasNext = stmt.getMoreResults();
                }
            }
        } catch (SQLException e) {
            logger.error("Database error in getTags!", e);
            return Collections.emptySet();
        }
        return results;
    }

    /**
     * This method removes all tags assigned to the given gif by the given user.
     *
     * @param userId The ID of the user
     * @param gifId  The ID of the gif to remove
     */
    public void removeGif(long userId, String gifId) {
        try (PreparedStatement stmt = con.prepareStatement("DELETE FROM tags WHERE user_id=? AND gif_id=?;")) {
            stmt.setLong(1, userId);
            stmt.setString(2, gifId);
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Database error in removeGif!", e);
        }
    }

    /**
     * This method gives the top 50 tags of a user.
     *
     * @param userId The ID of the user
     * @return A {@link Set<String>} of tags
     */
    public Set<String> listTags(long userId) {
        Set<String> results = new HashSet<>();

        try (PreparedStatement stmt = con.prepareStatement("SELECT tag, count(*) AS tag_count FROM tags WHERE user_id=? GROUP BY tag ORDER BY tag_count DESC LIMIT 50;")) {
            stmt.setLong(1, userId);

            // Execute and access results

            boolean hasNext = stmt.execute();
            while (hasNext) {
                try (ResultSet rs = stmt.getResultSet()) {
                    while (rs.next()) {
                        results.add(rs.getString("tag"));
                    }
                    hasNext = stmt.getMoreResults();
                }
            }
        } catch (SQLException e) {
            logger.error("Database error in listTags!", e);
            return Collections.emptySet();
        }
        return results;
    }

    // User functionality

    /**
     * This method adds a user to the database. If the user already exists, the user data will be updated.
     *
     * @param user The user to add or update
     */
    public void addOrUpdateUser(User user) {
        try (PreparedStatement stmt = con.prepareStatement("INSERT INTO users(id, first_name, last_name, username, language_code) VALUES(?,?,?,?,?) ON CONFLICT (id) DO UPDATE SET first_name=?, last_name=?, username=?, language_code=?;")) {
            // INSERT
            stmt.setLong(1, user.getId());
            stmt.setString(2, user.getFirstName());
            stmt.setString(3, user.getLastName());
            stmt.setString(4, user.getUserName());
            stmt.setString(5, user.getLanguageCode());
            // ON CONFLICT UPDATE
            stmt.setString(6, user.getFirstName());
            stmt.setString(7, user.getLastName());
            stmt.setString(8, user.getUserName());
            stmt.setString(9, user.getLanguageCode());
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Database error in addOrUpdateUser!", e);
        }
    }

    /**
     * This method adds a gif to the database. If the gif already exists, it will update the file id and gif set fields.
     *
     * @param gif The gif to add or update
     */
    public void addOrUpdateGif(Animation gif) {
        try (PreparedStatement stmt = con.prepareStatement("INSERT INTO gifs(file_unique_id, file_id) VALUES(?,?) ON CONFLICT (file_unique_id) DO UPDATE SET file_id=?;")) {
            // INSERT
            stmt.setString(1, gif.getFileUniqueId());
            stmt.setString(2, gif.getFileId());
            // ON CONFLICT UPDATE
            stmt.setString(3, gif.getFileId());
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Database error in addOrUpdateGif!", e);
        }
    }

    /**
     * This method exports all tag data for a user.
     *
     * @param user The user to export data for
     */
    public Set<TaggedGif> export(User user) {
        Set<TaggedGif> results = new HashSet<>();

        try (PreparedStatement stmt = con.prepareStatement("SELECT g.file_unique_id, g.file_id, t.tag_array FROM (SELECT gif_id, array_agg(tag) AS tag_array FROM tags WHERE user_id=? GROUP BY gif_id) t INNER JOIN gifs g ON t.gif_id = g.file_unique_id")) {
            stmt.setLong(1, user.getId());

            // Execute and access results

            boolean hasNext = stmt.execute();
            while (hasNext) {
                try (ResultSet rs = stmt.getResultSet()) {
                    while (rs.next()) {
                        List<String> tags = Arrays.asList((String[]) rs.getArray("tag_array").getArray());
                        results.add(new TaggedGif(rs.getString("file_unique_id"), rs.getString("file_id"), tags));
                    }
                    hasNext = stmt.getMoreResults();
                }
            }
        } catch (SQLException e) {
            logger.error("Database error in export!", e);
            return Collections.emptySet();
        }

        return results;
    }

    // Stats

    private int getUserCount() {
        int result = 0;
        try (Statement stmt = con.createStatement()) {
            stmt.execute("SELECT count(*) AS num FROM users;");
            try (ResultSet rs = stmt.getResultSet()) {
                if (rs.next()) {
                    result = rs.getInt("num");
                }
            }
        } catch (SQLException e) {
            return 0;
        }
        return result;
    }

    private int getGifCount() {
        int result = 0;
        try (Statement stmt = con.createStatement()) {
            stmt.execute("SELECT count(*) AS num FROM gifs;");
            try (ResultSet rs = stmt.getResultSet()) {
                if (rs.next()) {
                    result = rs.getInt("num");
                }
            }
        } catch (SQLException e) {
            return 0;
        }
        return result;
    }

    private int getTagCount() {
        int result = 0;
        try (Statement stmt = con.createStatement()) {
            stmt.execute("SELECT count(*) AS num FROM tags;");
            try (ResultSet rs = stmt.getResultSet()) {
                if (rs.next()) {
                    result = rs.getInt("num");
                }
            }
        } catch (SQLException e) {
            return 0;
        }
        return result;
    }

    /**
     * Deletes all user data, including gifs.
     *
     * @param user User whose data to delete
     */
    public void removeUser(User user) {
        try (PreparedStatement stmt = con.prepareStatement("DELETE FROM users WHERE id=?;")) {
            stmt.setLong(1, user.getId());
            stmt.executeUpdate();
        } catch (SQLException e) {
            logger.error("Database error in removeUser", e);
        }
    }

}
