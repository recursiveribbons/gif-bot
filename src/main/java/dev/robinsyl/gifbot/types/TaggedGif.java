package dev.robinsyl.gifbot.types;

import java.util.List;

public record TaggedGif(String id, String fileId, List<String> tags) {
}
