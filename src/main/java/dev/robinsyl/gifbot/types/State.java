package dev.robinsyl.gifbot.types;

import org.telegram.telegrambots.meta.api.objects.games.Animation;

public enum State {
    NONE(false, Object.class),
    SENT_GIF(true, Animation.class), DELETE_GIF(false, Object.class),
    CHECK_TAG(false, Object.class), SENT_TAGS(true, String.class),
    REMOVE_TAGS(false, Object.class), REMOVE_TAGS_SENT_GIF(true, String.class);

    private final boolean hasData;
    private final Class<?> dataType;

    State(boolean hasData, Class<?> dataType) {
        this.hasData = hasData;
        this.dataType = dataType;
    }

    public boolean hasData() {
        return hasData;
    }

    public Class<?> getDataType() {
        return dataType;
    }
}
