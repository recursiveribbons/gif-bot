package dev.robinsyl.gifbot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GifBotApplication {

    public static void main(String[] args) {
        SpringApplication.run(GifBotApplication.class, args);
    }

}
