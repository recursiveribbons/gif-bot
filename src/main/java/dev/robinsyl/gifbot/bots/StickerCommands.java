package dev.robinsyl.gifbot.bots;

import dev.robinsyl.gifbot.services.DatabaseService;
import dev.robinsyl.gifbot.services.StateService;
import dev.robinsyl.gifbot.types.State;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.abilitybots.api.bot.BaseAbilityBot;
import org.telegram.telegrambots.abilitybots.api.objects.Ability;
import org.telegram.telegrambots.abilitybots.api.objects.Flag;
import org.telegram.telegrambots.abilitybots.api.objects.Reply;
import org.telegram.telegrambots.abilitybots.api.util.AbilityExtension;
import org.telegram.telegrambots.meta.api.methods.AnswerInlineQuery;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.games.Animation;
import org.telegram.telegrambots.meta.api.objects.inlinequery.InlineQuery;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.InlineQueryResult;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.InlineQueryResultsButton;
import org.telegram.telegrambots.meta.api.objects.inlinequery.result.cached.InlineQueryResultCachedGif;

import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

import static org.telegram.telegrambots.abilitybots.api.objects.Flag.*;
import static org.telegram.telegrambots.abilitybots.api.objects.Locality.USER;
import static org.telegram.telegrambots.abilitybots.api.objects.Privacy.PUBLIC;

@Component
public class StickerCommands extends BaseCommands implements AbilityExtension {
    private static final Predicate<Update> GIF = u -> u.getMessage().hasAnimation();

    private final DatabaseService db;
    private final StateService states;
    private final Counter cancelCounter;
    private final Counter removeCounter;
    private final Counter removeTagsCounter;
    private final Counter checkTagsCounter;
    private final Counter queryCounter;

    @Autowired
    public StickerCommands(MeterRegistry meterRegistry, MessageSource messageSource, DatabaseService db, StateService states) {
        super(meterRegistry, messageSource);
        this.db = db;
        this.states = states;
        cancelCounter = createCounter("cancel");
        removeCounter = createCounter("remove");
        removeTagsCounter = createCounter("remove_tags");
        checkTagsCounter = createCounter("tags");
        queryCounter = createCounter("query");
    }

    @SuppressWarnings("unused")
    public Ability cancel() {
        return Ability.builder()
                .name("cancel")
                .info("Cancel the current operation")
                .locality(USER)
                .privacy(PUBLIC)
                .input(0)
                .action(ctx -> {
                    states.removeState(ctx.user());
                    sendString("bot.cancel", ctx);
                    db.addOrUpdateUser(ctx.user());
                })
                .post(ctx -> cancelCounter.increment())
                .build();
    }

    @SuppressWarnings("unused")
    public Ability receiveTags() {
        return Ability.builder()
                .name("default")
                .flag(MESSAGE, TEXT)
                .locality(USER)
                .privacy(PUBLIC)
                .action(ctx -> {
                    State state = states.getState(ctx.user()).orElse(State.NONE);
                    if (state == State.SENT_GIF) {
                        Animation gif = (Animation) states.getData(ctx.user()).orElseThrow(IllegalStateException::new);
                        states.removeState(ctx.user());
                        String[] tags = ctx.arguments();
                        db.addTags(ctx.user().getId(), gif, tags);
                        sendString("bot.gif.saved", ctx);
                    } else if (state == State.REMOVE_TAGS_SENT_GIF) {
                        String gifId = (String) states.getData(ctx.user()).orElseThrow(IllegalStateException::new);
                        states.removeState(ctx.user());
                        db.removeTags(ctx.user().getId(), gifId, ctx.arguments());
                        sendString("bot.gif.tags.removed", ctx);
                    } else {
                        states.setState(ctx.user(), State.SENT_TAGS, ctx.update().getMessage().getText());
                        sendString("bot.gif.tags.received", ctx);
                    }
                    db.addOrUpdateUser(ctx.user());
                })
                .build();
    }

    @SuppressWarnings("unused")
    public Ability remove() {
        return Ability.builder()
                .name("remove")
                .info("Remove a gif")
                .locality(USER)
                .privacy(PUBLIC)
                .action(ctx -> {
                    states.setState(ctx.user(), State.DELETE_GIF);
                    sendString("bot.gif.remove.send", ctx);
                    db.addOrUpdateUser(ctx.user());
                })
                .post(ctx -> removeCounter.increment())
                .build();
    }

    @SuppressWarnings("unused")
    public Ability removeTags() {
        return Ability.builder()
                .name("removetags")
                .info("Remove tags from a gif")
                .locality(USER)
                .privacy(PUBLIC)
                .action(ctx -> {
                    states.setState(ctx.user(), State.REMOVE_TAGS);
                    sendString("bot.gif.tags.remove.send.gif", ctx);
                    db.addOrUpdateUser(ctx.user());
                })
                .post(ctx -> removeTagsCounter.increment())
                .build();
    }

    @SuppressWarnings("unused")
    public Ability checkTags() {
        return Ability.builder()
                .name("tags")
                .info("Check gif tags")
                .locality(USER)
                .privacy(PUBLIC)
                .action(ctx -> {
                    states.setState(ctx.user(), State.CHECK_TAG);
                    sendString("bot.gif.tags.check", ctx);
                    db.addOrUpdateUser(ctx.user());
                })
                .post(ctx -> checkTagsCounter.increment())
                .build();
    }

    @SuppressWarnings("unused")
    public Reply receiveGif() {
        BiConsumer<BaseAbilityBot, Update> action = (bot, u) -> {
            State state = states.getState(getUser(u)).orElse(State.NONE);
            Animation gif = u.getMessage().getAnimation();
            switch (state) {
                case DELETE_GIF -> {
                    states.removeState(getUser(u));
                    db.removeGif(getUserId(u), getGifId(u));
                    sendString("bot.gif.removed", u, bot);
                }
                case REMOVE_TAGS -> {
                    states.setState(getUser(u), State.REMOVE_TAGS_SENT_GIF, getGifId(u));
                    sendString("bot.gif.tags.remove.send.tags", u, bot);
                }
                case CHECK_TAG -> {
                    states.removeState(getUser(u));
                    Set<String> tags = db.getTags(getUserId(u), getGifId(u));
                    if (tags.isEmpty()) {
                        sendString("bot.gif.tags.empty", u, bot);
                    } else {
                        sendString("bot.gif.tags", u, bot, String.join(" ", tags));
                    }
                }
                case SENT_TAGS -> {
                    String tagString = (String) states.getData(getUser(u)).orElseThrow(IllegalStateException::new);
                    String[] t = tagString.split(" ");
                    states.removeState(getUser(u));
                    db.addTags(getUserId(u), getGif(u), t);
                    sendString("bot.gif.saved", u, bot);
                }
                default -> {
                    states.setState(getUser(u), State.SENT_GIF, getGif(u));
                    db.getTaggedGif(getUserId(u), getGifId(u))
                            .ifPresent(s -> sendString("bot.gif.tags", u, bot, String.join(" ", s.tags())));
                    sendString("bot.gif.tags.send.tags", u, bot);
                }
            }
            db.addOrUpdateUser(getUser(u));
        };
        return Reply.of(action, Flag.MESSAGE, GIF);
    }

    @SuppressWarnings("unused")
    public Reply processInlineQuery() {
        BiConsumer<BaseAbilityBot, Update> action = (bot, u) -> {
            queryCounter.increment();
            InlineQuery query = u.getInlineQuery();
            String[] tags = query.getQuery().split(" ");
            int offset = getOffset(query);
            List<InlineQueryResult> results = db.getGifFileIds(query.getFrom().getId(), offset, tags)
                    .stream()
                    .<InlineQueryResult>map(id -> new InlineQueryResultCachedGif(id.substring(0, Math.min(id.length(), 64)), id))
                    .toList();
            AnswerInlineQuery answer = createAnswer(u, results);
            bot.getSilent().execute(answer);
        };
        return Reply.of(action, INLINE_QUERY);
    }

    private AnswerInlineQuery createAnswer(Update update, List<InlineQueryResult> results) {
        InlineQuery query = update.getInlineQuery();
        InlineQueryResultsButton button = InlineQueryResultsButton.builder()
                .text(getString("bot.sticker.add", update))
                .startParameter("addsticker")
                .build();
        AnswerInlineQuery answer = AnswerInlineQuery.builder()
                .inlineQueryId(query.getId())
                .results(results)
                .isPersonal(true)
                .button(button)
                .cacheTime(10) // seconds
                .build();
        if (results.size() > 50) {
            String newOffset = String.valueOf(getOffset(query) + results.size());
            answer.setNextOffset(newOffset);
        }
        return answer;
    }

    private int getOffset(InlineQuery query) {
        try {
            return Integer.parseInt(query.getOffset());
        } catch (NumberFormatException | NullPointerException e) {
            return 0;
        }
    }

    private void sendString(String key, Update update, BaseAbilityBot bot, Object... args) {
        bot.getSilent().sendMd(getString(key, update, args), update.getMessage().getChatId());
    }

    private Animation getGif(Update update) {
        return update.getMessage().getAnimation();
    }

    private String getGifId(Update update) {
        return update.getMessage().getAnimation().getFileUniqueId();
    }

    private User getUser(Update update) {
        return update.getMessage().getFrom();
    }

    private long getUserId(Update update) {
        return update.getMessage().getFrom().getId();
    }
}
