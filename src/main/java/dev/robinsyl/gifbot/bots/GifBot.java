package dev.robinsyl.gifbot.bots;

import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.abilitybots.api.bot.AbilityBot;
import org.telegram.telegrambots.abilitybots.api.objects.Ability;
import org.telegram.telegrambots.abilitybots.api.objects.Flag;
import org.telegram.telegrambots.abilitybots.api.util.AbilityExtension;
import org.telegram.telegrambots.client.okhttp.OkHttpTelegramClient;
import org.telegram.telegrambots.longpolling.interfaces.LongPollingUpdateConsumer;
import org.telegram.telegrambots.longpolling.starter.SpringLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.List;

import static org.telegram.telegrambots.abilitybots.api.objects.Flag.CALLBACK_QUERY;
import static org.telegram.telegrambots.abilitybots.api.objects.Flag.INLINE_QUERY;
import static org.telegram.telegrambots.abilitybots.api.objects.Locality.ALL;
import static org.telegram.telegrambots.abilitybots.api.objects.Privacy.PUBLIC;

@Component
public class GifBot extends AbilityBot implements SpringLongPollingBot {
    private static final Logger logger = LoggerFactory.getLogger(GifBot.class);

    private final String botToken;
    private final long creatorId;

    @Autowired
    public GifBot(@Value("${gifs.bot.token}") String botToken, @Value("${gifs.bot.username}") String botUsername, @Value("${gifs.bot.creator.id}") long creatorId, List<AbilityExtension> extensions) {
        super(new OkHttpTelegramClient(botToken), botUsername);
        this.botToken = botToken;
        this.creatorId = creatorId;
        if (!extensions.isEmpty()) {
            addExtensions(extensions);
            logger.info("Loaded extensions: {}", extensions);
        }
    }

    @SuppressWarnings("unused")
    public Ability ping() {
        return Ability.builder()
                .name("ping")
                .info("ping pong")
                .locality(ALL)
                .privacy(PUBLIC)
                .input(0)
                .action(ctx -> silent.send("pong", ctx.chatId()))
                .build();
    }

    @Override
    public long creatorId() {
        return creatorId;
    }

    @Override
    @PostConstruct
    public void onRegister() {
        super.onRegister();
        silent.send("Bot has been restarted.", creatorId);
    }

    @Override
    public boolean checkGlobalFlags(Update update) {
        return Flag.MESSAGE.test(update) || INLINE_QUERY.test(update) || CALLBACK_QUERY.test(update);
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    @Override
    public LongPollingUpdateConsumer getUpdatesConsumer() {
        return this;
    }
}
