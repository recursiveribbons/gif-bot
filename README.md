# GIF bot
A Telegram bot for tagging GIFs and posting them via inline bot. Currently located under https://t.me/QuickGifBot.

## Usage
_This guide is for how to run an instance of the bot. For help on using the bot, run the `/help` command in the bot._

### From the registry
1. Create a new bot with the [BotFather](https://t.me/BotFather)
2. Make a copy of [compose.yaml](compose.yaml) on your server and fill in the environment variables
3. Run `docker-compose up -d`

### Build the container yourself
Same steps as _From the registry_, but with the file [compose.build.yaml](compose.build.yaml).

### Environment variables
- `GIFS_BOT_TOKEN`: The bot token retrieved from the BotFather
- `GIFS_BOT_USERNAME`: The username you gave the bot, without the @ symbol.
- `GIFS_BOT_CREATOR_ID`: Your user ID. You can retrieve this via the various user ID bots on Telegram
- `GIFS_DB_PASSWORD`: The database password, must match `POSTGRES_PASSWORD`.
